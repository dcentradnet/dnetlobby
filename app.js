const express = require('express');
const bodyParser = require('body-parser');
const mongoist = require('mongoist');
const ECKey = require('ec-key');

const PORT = process.env.PORT;
const DB_URL = process.env.DB_URL;

const db = mongoist(DB_URL);

const app = express();
app.use(bodyParser.json());

// setup indexing and uniqueness
db.consortia.createIndex({cid: 1}, {unique: true});
db.consortia.createIndex({name: 1}, {unique: true});
db.requests.createIndex({cid: 1, rpub: 1}, {unique: true});
db.approvals.createIndex({cid: 1, rpub: 1, pub: 1}, {unique: true});
//db.members.createIndex({cid: 1, pub: 1}, {unique: true});

// see https://fabric-sdk-node.github.io/release-1.4/tutorial-sign-transaction-offline.html
// by default, the fabric client will use 'SHA2' with key size 256.

function sign(msg, priv) {
  const key = new ECKey(priv, 'pem');
  return key.createSign('SHA256').update((JSON.stringify(msg))).sign('base64');
}

function verify(msg, sig, pub) {
  const key = new ECKey(pub, 'pem');
  return key.createVerify('SHA256').update(JSON.stringify(msg)).verify(sig, 'base64');
}

async function isRequestConfirmed(cid, rpub) {
  const numMembers = await db.members.count({cid: cid, rpub: rpub});
  const numApprovals = await db.approvals.count({cid: cid, rpub: rpub});
  
  return numApprovals / numMembers > 2/3;
}

app.get('/api/consortia', async (req, res) => {
  const consortia = await db.consortia.find();
  res.send(consortia);
});

/*
body = {
  msg: {},
  pub: string,
  sig: string
}
*/
app.post('/api/consortia/add', async (req, res) => {
  const body = req.body;
  const msg = body.msg;
  const sig = body.sig;
  const pub = body.pub;

  if(!verify(msg, sig, pub)) {
    return res.status(400).send();
  }

  await db.consortia.insert(Object.assign({}, msg, {founder: pub}));
  await db.members.insert({cid: msg.cid, pub: pub});
  
  res.status(200).send();
});

app.post('/api/consortia/remove', (req, res) => {
  const body = req.body;
  const msg = body.msg;
  const sig = body.sig;
  const pub = body.pub;
  
  if(!verify(msg, sig, pub)) {
    return res.status(400).send();
  }
  
  // FIXME a consortia can only be removed if ?
  
  res.status(200).send();
});

app.get('/api/requests', async (req, res) => {
  const cid = req.query.cid;
  const requests = await db.requests.find(cid ? {cid: cid} : {});
  res.send(requests);
});

app.post('/api/requests/add', async (req, res) => {
  const body = req.body;
  const msg = body.msg;
  const sig = body.sig;
  const pub = body.pub;
  
  if(!verify(msg, sig, pub)) {
    return res.status(400).send();
  }

  await db.requests.insert({cid: msg.cid, rpub: pub, sig: sig});

  res.status(200).send();
});

app.post('/api/requests/remove', async (req, res) => {
  const body = req.body;
  const msg = body.msg;
  const sig = body.sig;
  const pub = body.pub;
  
  if(!verify(msg, sig, pub)) {
    return res.status(400).send();
  }

  // remove request
  await db.approvals.remove({cid: msg.cid, rpub: pub, sig: sig}, true);
  
  res.status(200).send();
});

app.get('/api/members', async (req, res) => {
  const cid = req.query.cid;
  const members = await db.members.find(cid ? {cid: cid} : {});
  res.send(members);
});

app.post('/api/approvals/confirm', async (req, res) => {
  const body = req.body;
  const msg = body.msg;
  const sig = body.sig;
  const pub = body.pub;
  
  if(!verify(msg, sig, pub)) {
    return res.status(400).send();
  }
  
  const request = await db.requests.findOne({cid: msg.cid, rpub: msg.rpub});
  // check if request exists
  if(!request) {
    return res.status(400).send({message: 'Request not found!'});
  }
  
  const amember = await db.members.findOne({cid: msg.cid, pub: pub});
  // check if approver is a member
  if(!amember) {
    return res.status(400).send({message: 'Approver is not a member!'});
  }
  
  // insert approval
  await db.approvals.insert({cid: msg.cid, rpub: msg.rpub, pub: pub, sig: sig});
  
  // add as member if 2/3 majority is reached
  //if(await isRequestConfirmed(msg.cid, msg.rpub)) {
    // store member with encrypted swarm token
    await db.members.insert({cid: msg.cid, pub: msg.rpub, token: msg.token, apub: pub});
  //}
  
  res.status(200).send();
});

app.post('/api/approvals/remove', async (req, res) => {
  const body = req.body;
  const msg = body.msg;
  const sig = body.sig;
  const pub = body.pub;
  
  if(!verify(msg, sig, pub)) {
    return res.status(400).send();
  }
  
  const amember = await db.members.findOne({cid: msg.cid, pub: pub});
  // check if approver is a member
  if(!amember) {
    return res.status(400).send();
  }
  
  // remove approval
  await db.approvals.remove({cid: msg.cid, rpub: msg.rpub, pub: pub, sig: sig}, true);
  
  res.status(200).send();
});

app.post('/api/approvals/confirmed', async (req, res) => {
  const body = req.body;
  const cid = body.cid;
  const rpub = body.rpub;

  const isConfirmed = await isRequestConfirmed(cid, rpub);
  if(!isConfirmed) {
    return res.status(200).send({isConfirmed: false});
  }
  
  const member = await db.members.findOne({cid: cid, pub: rpub});
  res.status(200).send({
    isConfirmed: isConfirmed,
    token: member.token
  });
});

app.post('/api/name/get', async (req, res) => {
  const pub = req.body.pub;
  
  const name = await db.names.findOne({pub: pub});
  
  res.status(200).send(name);
});

app.post('/api/name/put', async (req, res) => {
  const body = req.body;
  const msg = body.msg;
  const sig = body.sig;
  const pub = body.pub;

  if(!verify(msg, sig, pub)) {
    return res.status(400).send();
  }
  
  await db.names.update({pub: pub}, {$set: {msg: msg, sig: sig}}, {upsert: true});
  
  res.status(200).send({message: 'Store successful!'});
});

app.listen(PORT, () => console.log(`dNet Lobby server listening on port ${PORT}!`));

module.exports = app;