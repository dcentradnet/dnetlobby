FROM node:10

RUN mkdir -p /home/node/app/
WORKDIR /home/node/app/

COPY package.json package-lock.json app.js ./
RUN npm install

CMD ["npm", "start"]