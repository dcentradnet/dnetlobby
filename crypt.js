const crypto = require('crypto');

exports.encrypt = function(text, passphrase) {
  const cipher = crypto.createCipher('aes-256-cbc', passphrase);
  let crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  
  return crypted;
}

exports.decrypt = function(text, passphrase) {
  const decipher = crypto.createDecipher('aes-256-cbc', passphrase);
  let dec = decipher.update(text, 'hex', 'utf8');
  dec += decipher.final('utf8');
  
  return dec;
}