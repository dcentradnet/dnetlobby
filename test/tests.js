const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoUnit = require('mongo-unit');
const ECKey = require('ec-key');

const crypt = require('../crypt');
const app = require('../app');

const expect = chai.expect;
chai.use(chaiHttp);

const member1Key = new ECKey(
'-----BEGIN PRIVATE KEY-----\n'+
'MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQguPU8/JIgHlwfB+B3\n'+
'CymDAn3eTQYEzdVf1GlijukXBfOhRANCAAR9WqiBtyt4GJH7VdP/jOZsdrsgCyY7\n'+
'kYw27jzQTuzwXW0wVNRu9Mi9AY+X22OxTbxv63gE0L1GT+gRsUIDBj2J\n'+
'-----END PRIVATE KEY-----'
);

//const member1Key = ECKey.createECKey('P-256');
const member2Key = ECKey.createECKey('P-256');

describe('users', () => {
 //beforeEach(() => mongoUnit.start());
 //afterEach(() => mongoUnit.drop());
 
 const requester = chai.request(app).keepOpen();
 
 it('should add consortia', async () => {
   const member1Sign = member1Key.createSign('SHA256');
  
   const msg = {cid: 'Greatness'};
   const req = {
    msg: msg,
    sig: member1Sign.update((JSON.stringify(msg))).sign('base64'),
    pub: member1Key.asPublicECKey().toString()
   };
   
   let res;
   res = await requester.post('/api/consortia/add').send(req);
   
   expect(res.status).to.equal(200);
   console.log(res.body);
   
   res = await requester.get('/api/consortia').send();
   console.log(res.body);
   
   const cons = res.body[0];
   expect(cons.cid).to.equal(msg.cid);
   expect(cons.founder).to.equal(req.pub);
   //expect(cons.sig).to.equal(req.sig);
 })
 
 it('should add request', async () => {
   const member2Sign = member2Key.createSign('SHA256');
   
   const msg = {cid: 'Greatness'};
   
   let res;
   res = await requester.post('/api/requests/add').send({
    msg: msg,
    sig: member2Sign.update((JSON.stringify(msg))).sign('base64'),
    pub: member2Key.asPublicECKey().toString()
   });
   
   expect(res.status).to.equal(200);
   console.log(res.body);
   
   res = await requester.get('/api/requests').send();
   console.log(res.body);
 })
 
 it('should confirm request', async () => {
   const member1Sign = member1Key.createSign('SHA256');
   
   // compute secret
   const secret = member1Key.computeSecret(member2Key.asPublicECKey());
   
   // encrypt token
   const token = 'docker-swarm-access-token';
   const encryptedToken = crypt.encrypt(token, secret);
  
   const msg = {
    cid: 'Greatness',
    rpub: member2Key.asPublicECKey().toString(),
    token: encryptedToken
   };
   
   let res;
   res = await requester.post('/api/approvals/confirm').send({
    msg: msg,
    sig: member1Sign.update((JSON.stringify(msg))).sign('base64'),
    pub: member1Key.asPublicECKey().toString()
   });
   
   expect(res.status).to.equal(200);
   console.log(res.body);
   
   // check request approval confirmation
   res = await requester.post('/api/approvals/confirmed').send({
    cid: msg.cid,
    rpub: msg.rpub
   });
   console.log(res.body);
   expect(res.body.isConfirmed).to.equal(true);

   // check membership
   res = await requester.get('/api/members?cid='+msg.cid).send();
   console.log(res.body);
   
   const member = res.body[1];
   expect(member.cid).to.equal(msg.cid);
   expect(member.pub).to.equal(msg.rpub);
 })
 
 it('should put name', async () => {
   const member2Sign = member2Key.createSign('SHA256');
   
   const msg = {name: 'Mr. Proper'};
   
   let res;
   res = await requester.post('/api/name/put').send({
     msg: msg,
     sig: member2Sign.update((JSON.stringify(msg))).sign('base64'),
     pub: member2Key.asPublicECKey().toString()
   });
   expect(res.status).to.equal(200);
   console.log(res.body);
   
   res = await requester.post('/api/name/get').send({
     pub: member2Key.asPublicECKey().toString()
   });
   expect(res.status).to.equal(200);
   console.log(res.body);
   
   expect(res.body.msg.name).to.equal(msg.name);
 })

})